#include "reeti.h"

/*
 * This samples allow to  launch an existing pose or sequence on the REETI.
 */

int main(int argc,char **argv){
    /* Create an instance of reeti and connect to the chosen reeti */
    Reeti reeti("localhost");
    /* Check if the connection is done */
    if(reeti.isConnected()){
        reeti.playPoseSync("Ears/earsBottom",1000);
        sleep(1);
        reeti.playSequenceSync("ReetiSeq/dance",1000);
        sleep(1);
    }
    return 0;
}
