#include "reeti.h"

/*
 * The program start by setting all the servo positions.
 * And retrieve the last position of the Reeti
 */

int main(int argc,char **argv){
    /* Create an instance of reeti and connect to the chosen reeti */
    Reeti reeti("localhost");
    /* Check if the connection is done */
    if(reeti.isConnected()){
        ReetiPosition pos(20,20,20,20,20,20,20,20,20,20,20,20,20,20,20);
        reeti.setPoseSync(pos,25,1000);
        int i =0;
        while(i<5){
            pos = reeti.getLastPosition();
            std::cout <<"neckRotat: " << pos.neckRotat << "\n"
                     <<"neckPan: " << pos.neckPan << "\n"
                    <<"neckTilt: " << pos.neckTilt << "\n"
                   <<"rightLC: " << pos.rightLC << "\n"
                  <<"leftLC: " << pos.leftLC << "\n"
                 <<"topLip: " << pos.topLip << "\n"
                <<"bottomLip: " << pos.bottomLip << "\n"
               <<"rightEyePan: " << pos.rightEyePan << "\n"
              <<"rightEyeTilt: " << pos.rightEyeTilt << "\n"
             <<"leftEyePan: " << pos.leftEyePan << "\n"
            <<"leftEyeTilt: " << pos.leftEyeTilt << "\n"
            <<"rightEyeLid: " << pos.rightEyeLid << "\n"
            <<"leftEyeLid: " << pos.leftEyeLid << "\n"
            <<"rightEar: " << pos.rightEar << "\n"
            <<"leftEar: " << pos.leftEar << "\n" << std::endl;
            sleep(1);
            i++;
        }
    }
}
