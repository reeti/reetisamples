#include "reeti.h"

/*
 * The program make the reeti talk and move its lips.
 * And retrieve the bookmars and if the reeti is speaking
 */


int main(int argc,char **argv){
    /* Create an instance of reeti and connect to the chosen reeti */
    Reeti reeti("localhost");
    /* Check if the connection is done */
    if(reeti.isConnected()){
        reeti.sayWithSynchroSync("Bonjour, je m'appelle Reeti \\\\book=name , je test actuellement\\\\book=current les bookmarks et detecte quand j'arrete de parler",1000);
        int i=0;
        while(i<10){
            std::cout<< "Bookmark: " << reeti.getLastBookmark() << std::endl;
            std::cout <<"Speaking: "<< reeti.isSpeaking() << std::endl;
            sleep(1);
            i++;
        }
    }
    return 0;
}
