#include "reeti.h"

/*
 * This samples subscribes to the isSpeaking and BookMark publishers.
 * Those allows to be informed when a reeti is speaking or when a bookmark is found in the text.
 * The node also call the SayWithSynchro service in order to make the reeti talk.
 */

Reeti *reeti;

/* The asynchronous services callback, let you know if it was correctly done */
void serviceCallback(ErrorCode _err){
    if(_err==OK){
        std::cout << "service ok" << std::endl;
    }else if(_err==TIMEOUT){
        std::cout << "service timed out" << std::endl;
    }else if(_err==ERROR){
        std::cout << "error" << std::endl;
    }
}
/*
 * Creation of the services that will get the information from reetiros
 */
void bookmark(ReetiBookmark _book){
    std::cout << "Found Bookmark: " << _book.bookmark << std::endl;
}
/*
 * Creation of the callback linked to the subscriber in order to know when the Reeti is talking
 */
void isSpeaking(ReetiSpeaking _speak){
    std::cout << "Reeti Speaking: "<< _speak.isSpeaking<< std::endl;
}

int main(int argc,char **argv){
    /* Create an instance of reeti and connect to the chosen reeti */
    reeti = new Reeti("localhost");
    /* Check if the connection is done */
    if(reeti->isConnected()){
        reeti->registerCallbackBookMark(bookmark);
        reeti->registerCallbackIsSpeaking(isSpeaking);
        reeti->sayWithSynchroAsync("Bonjour, je m'appelle Reeti \\\\book=name , je test actuellement\\\\book=current les bookmarks et detecte quand j'arrete de parler",serviceCallback,1000);
    }
    char c;
    std::cin >> c;
    return 0;
}
