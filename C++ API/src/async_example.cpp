#include "reeti.h"

Reeti *reeti;
int count = 0;
/* A simple example that show how to use the asynchronous functions of the reeti api.
 * The example will make the reeti move wait for him to reach a position, then make
 * him talk until a bookmark is reached and then change the led color.
 * */

/* The asynchronous services callback, let you know if it was correctly done */
void serviceCallback(ErrorCode _err){
    if(_err==OK){
        std::cout << "service ok" << std::endl;
    }else if(_err==TIMEOUT){
        std::cout << "service timed out" << std::endl;
    }else if(_err==ERROR){
        std::cout << "error" << std::endl;
    }
}
/* The callback on the bookmarks found in a reeti speech */
void bookmarkcallback(ReetiBookmark _book){
    if(_book.bookmark == "color"){
        /* set the led color */
        reeti->setLedColor(RIGHTLED,"blue");
        reeti->setLedColor(LEFTLED,"red");
    }
}
/* The callback on the position of the reeti */
void positioncallback(ReetiPosition _pos){
    if(_pos.neckRotat < 6 && count ==0){
        count = 1;
        /* make the reeti talk and move its lips */
        reeti->sayWithSynchroAsync("\\language=English \\voice=Simon I reached the position, I will now \\book=color change my led color",serviceCallback,1000);
    }
}
/* The callback on the led color of the reeti */
void ledcolorcallback(ReetiLedColor _color){
    if(_color.color == "red"){
        reeti->sayWithSynchroAsync("\\language=English \\voice=Simon I do prefer the green color",serviceCallback,1000);
        sleep(3);
        reeti->setLedColor(BOTHLED,"green");
    }
}

int main(int argc, char **argv){
    /* Create an instance of reeti and connect to the chosen reeti */
    reeti = new Reeti("localhost");
    /* Check if the connection is done */
    if(reeti->isConnected()){
        /* Creation of a ReetiPosition to store the position of the 15 actuators */
        ReetiPosition pos(5,50,50,50,50,50,50,50,50,50,50,100,100,50,50);
        /* Subscribe to the position, led color and bookmarks of the reeti to retrieve the values */
        reeti->registerCallbackPosition(positioncallback);
        reeti->registerCallbackLedColor(ledcolorcallback);
        reeti->registerCallbackBookMark(bookmarkcallback);
        /* Set the position of the reeti to the ReetiPosition defined above */
        reeti->setPoseAsync(pos,40,serviceCallback,1000);
    }
    char c;
    std::cin >> c;
}
