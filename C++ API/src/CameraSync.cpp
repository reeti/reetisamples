#include "reeti.h"

/*
 * This sample show you how to take a picture, record a video and stop the recording
 */

int main(int argc,char **argv){
    /* Create an instance of reeti and connect to the chosen reeti */
    Reeti reeti("localhost");
    /* Check if the connection is done */
    if(reeti.isConnected()){
        reeti.takePictureSync(RIGHTCAM,"syncpicturetestc++",1000);
        sleep(1);
        reeti.recordVideoSync(LEFTCAM,"syncvideotestc++",1000);
        sleep(4);
        reeti.stopRecordSync(LEFTCAM,1000);
        sleep(1);
    }
    return 0;
}
