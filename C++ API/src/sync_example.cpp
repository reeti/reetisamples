#include "reeti.h"

/* A simple example showing how to use the synchronous function of the reeti api.
 * The reeti will take a picture, then move to a position, start recording a video
 * and move his head while recording before stopping the record.
 * */


int main (int argc, char **argv){
    /* Create an instance of reeti and connect to the chosen reeti */
    Reeti reeti("localhost");
    /* Check if the connection is done */
    if(reeti.isConnected()){
        /* take a picture with the left camera of the reeti */
        reeti.takePictureSync(LEFTCAM,"test_picture",1000);
        sleep(1);
        /* create a ReetiPosition to store the 15 position of the actuators */
        ReetiPosition pos(50,50,50,50,50,50,50,50,50,50,50,100,100,50,50);
        /* Set the position of the reeti to the one defined above */
        reeti.setPoseSync(pos,40,1000);
        sleep(1);
        /* Start the recording of the video with the right camera */
        reeti.recordVideoSync(RIGHTCAM,"test_video",1000);
        sleep(1);
        /* change the neckRotat actuator position of the ReetiPosition*/
        pos.neckRotat=95;
        /* Set the position of the reeti to the new one */
        reeti.setPoseSync(pos,20,1000);
        /* Wait until the desired position is reached */
        while(reeti.getLastPosition().neckRotat!=95){
            usleep(300);
        }
        sleep(1);
        pos.neckRotat=5;
        reeti.setPoseSync(pos,20,1000);
        while(reeti.getLastPosition().neckRotat!=5){
            usleep(300);
        }
        /* Stop the recording of the video */
        reeti.stopRecordSync(RIGHTCAM,1000);
    }
    sleep(1);
    return 0;
}
