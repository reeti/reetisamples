#include "reeti.h"

/*
 * This sample subscribes to the Reeti servo's positions.
 * The program start by setting all the servo positions.
 * Then, you can make your reeti moving with RPilot, and you will see the callback "GetPosition" triggered.
 */

Reeti *reeti;
ReetiPosition prevPos;
/* The callback on the position of the reeti */
void position(ReetiPosition _pos){
    if(_pos!=prevPos){
        std::cout <<"neckRotat: " << _pos.neckRotat << "\n"
                 <<"neckPan: " << _pos.neckPan << "\n"
                <<"neckTilt: " << _pos.neckTilt << "\n"
               <<"rightLC: " << _pos.rightLC << "\n"
              <<"leftLC: " << _pos.leftLC << "\n"
             <<"topLip: " << _pos.topLip << "\n"
            <<"bottomLip: " << _pos.bottomLip << "\n"
           <<"rightEyePan: " << _pos.rightEyePan << "\n"
          <<"rightEyeTilt: " << _pos.rightEyeTilt << "\n"
         <<"leftEyePan: " << _pos.leftEyePan << "\n"
        <<"leftEyeTilt: " << _pos.leftEyeTilt << "\n"
        <<"rightEyeLid: " << _pos.rightEyeLid << "\n"
        <<"leftEyeLid: " << _pos.leftEyeLid << "\n"
        <<"rightEar: " << _pos.rightEar << "\n"
        <<"leftEar: " << _pos.leftEar << "\n" << std::endl;
    }
    prevPos = _pos;
}
/* The asynchronous services callback, let you know if it was correctly done */
void serviceCallback(ErrorCode _err){
    if(_err==OK){
        std::cout << "service ok" << std::endl;
    }else if(_err==TIMEOUT){
        std::cout << "service timed out" << std::endl;
    }else if(_err==ERROR){
        std::cout << "error" << std::endl;
    }
}
int main(int argc,char **argv){
    /* Create an instance of reeti and connect to the chosen reeti */
    reeti = new Reeti("localhost");
    /* Check if the connection is done */
    if(reeti->isConnected()){
        reeti->registerCallbackPosition(position);
        ReetiPosition pos(20,20,20,20,20,20,20,20,20,20,20,20,20,20,20);
        reeti->setPoseAsync(pos,25,serviceCallback,1000);
    }
    char c;
    std::cin>>c;
    return 0;
}
