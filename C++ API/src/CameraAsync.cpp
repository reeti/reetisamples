#include <chrono>
#include "reeti.h"
/*
 * This sample show you how to take a picture, record a video and stop the recording
 */

Reeti *reeti;
bool recordStarted = false;

/* The asynchronous services callback, let you know if it was correctly done */
void serviceCallback(ErrorCode _err){
    if(_err==OK){
        std::cout << "service ok" << std::endl;
        /* If the record video is launched in the main we wait some time and stop it after */
        if(recordStarted == true){
            for(auto runUntil = std::chrono::system_clock::now()+ std::chrono::seconds(4);std::chrono::system_clock::now() < runUntil;)
            {
            }
            reeti->stopRecordAsync(LEFTCAM,serviceCallback,1000);
            recordStarted = false;
        }
    }else if(_err==TIMEOUT){
        std::cout << "service timed out" << std::endl;
        if(recordStarted == true){
            recordStarted = false;
        }
    }else if(_err==ERROR){
        std::cout << "error" << std::endl;
        if(recordStarted == true){
            recordStarted = false;
        }
    }
}

int main(int argc,char **argv){
    /* Create an instance of reeti and connect to the chosen reeti */
    reeti = new Reeti("localhost");
    /* Check if the connection is done */
    if(reeti->isConnected()){
        reeti->takePictureAsync(RIGHTCAM,"asyncpicturetestc++",serviceCallback,1000);
        sleep(1);
        reeti->recordVideoAsync(LEFTCAM,"asyncvideotestc++",serviceCallback,1000);
        recordStarted = true;
    }
    char c;
    std::cin >> c;
    return 0;
}
