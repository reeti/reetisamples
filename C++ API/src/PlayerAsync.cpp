#include "reeti.h"

/*
 * This samples allow to  launch an existing pose or sequence on the REETI.
 */

Reeti *reeti;
bool posePlayed = false;
/* The asynchronous services callback, let you know if it was correctly done */
void serviceCallback(ErrorCode _err){
    if(_err==OK){
        std::cout << "service ok" << std::endl;
        if(posePlayed ==true){
            sleep(2);
            reeti->playSequenceAsync("ReetiSeq/dance",serviceCallback,1000);
            posePlayed =false;
        }
    }else if(_err==TIMEOUT){
        std::cout << "service timed out" << std::endl;

    }else if(_err==ERROR){
        std::cout << "error" << std::endl;
    }
}

int main(int argc,char **argv){
    /* Create an instance of reeti and connect to the chosen reeti */
    reeti = new Reeti("localhost");
    /* Check if the connection is done */
    if(reeti->isConnected()){
        reeti->playPoseAsync("Ears/earsBottom",serviceCallback,1000);
        posePlayed = true;
    }
    char c;
    std::cin >> c;
    return 0;
}
