import java.lang.*;
import java.io.*;
import java.net.*;

public class ServoState {
	public static Socket client;
	public static BufferedReader in ;
	public static PrintWriter out; 
	
	public static void main(String[] args){
		 /* Connection to Urbi, by default the Urbi's port is set to 54001 */
		connectToHost("localhost",54001);
		/* Definition of the command to Send to Urbi.
	     * In order to retrieve the variable it must have been binded
	     * in your UModule using UBindVar(UModuleName,VarName) 
	     * We ask Urbi about the Variable and print the answer*/
		System.out.println("Neck Rotation: "+sendMessageAndWaitForReply("Global.servo.neckRotat;")+"%");
		System.out.println("Neck Tilt: "+sendMessageAndWaitForReply("Global.servo.neckTilt;")+"%");
		System.out.println("Neck Pan: "+sendMessageAndWaitForReply("Global.servo.neckPan;")+"%");
		System.out.println("Left Ear: "+sendMessageAndWaitForReply("Global.servo.leftEar;")+"%");
		System.out.println("Right Ear: "+sendMessageAndWaitForReply("Global.servo.rightEar;")+"%");
		System.out.println("Cheeks Color: "+sendMessageAndWaitForReply("Global.servo.color;"));
	}
	
	/*Creation of the TCPIP connection with URBI*/
	public static void connectToHost(String _ip,int _port){
		try{
			client = new Socket(_ip,_port);
			in = new BufferedReader(new InputStreamReader(client.getInputStream()));
			out = new PrintWriter(client.getOutputStream(),true);
			System.out.println("Connexion Established");
			waitHeaderReception();
		}
		catch(Exception e){
			System.out.println("connexion to REETI impossible");
		}
	}
	
	/* We wait that our application has received the header sent by URBI to proceed:
	 * [00990763] *** ********************************************************
	 * [00990771] *** Urbi SDK version 2.6 rev. d949607
	 * [00990772] *** Copyright (C) 2004-2011 Gostai S.A.S.
	 * [00990772] *** ....
	 * [00990773] *** Check our community site: http://www.urbiforge.org.
	 * [00990773] *** ********************************************************
	 */
	public static void waitHeaderReception(){
		int starLineCount =0;
		try{
			
			while(starLineCount <= 1){
				while(!in.ready()){
				}
				String line = in.readLine();
				if(line.contains("*********")) starLineCount++;
			}
			System.out.print("Header Received\n");
		}
		catch(Exception e){
			System.out.println(e);
		}
	}
	
	/*Send the command to Urbi and wait for it to reply*/
	public static String sendMessageAndWaitForReply(String _msg){
		try{
			out.print(_msg+"\n");
			out.flush();
			String line = in.readLine();
			/*Remove the time stamp of the Urbi's answer before returning it*/
			return line.substring(11);
		}
		catch(Exception e){
			System.out.println(e);
			return "error";	
		}
	}
}

