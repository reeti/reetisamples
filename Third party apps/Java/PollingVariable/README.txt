How to compile:
- open this project in eclipse (File -> Import -> Existing projects)
- build

How to launch:
- eclipse :
  * press the launch button
- command line:
  * cd bin
  * java PollingVariable
