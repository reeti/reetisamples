import java.io.*;
import java.net.*;
import java.util.Timer;
import java.util.TimerTask;

/*
 * Simple polling example.
 * This will connect to the urbi console server, move the the left eye servo motor to the left, then slowly move it to the right.
 * During the movement, it will print periodically the position of the servo.
 */


public class PollingVariable {
	public static Socket client;
	public static BufferedReader in ;
	public static PrintWriter out; 
	
	public static void main(String[] args){
		connectToHost("localhost",54001);
		sendMessageAndWaitForReply("Global.servo.leftEyePan = 0 speed: 50;echo(1);"); // go to position 0, and wait till we reached it
		sendMessage("Global.servo.leftEyePan = 100 speed: 25,");
		pollServoPosition();
	}
	/*Creation of the TCPIP connection with URBI*/
	public static void connectToHost(String _ip,int _port){
		try{
			client = new Socket(_ip,_port);
			in = new BufferedReader(new InputStreamReader(client.getInputStream()));
			out = new PrintWriter(client.getOutputStream(),true);
			System.out.println("Connexion Established");
			waitHeaderReception();
		}
		catch(Exception e){
			System.out.println("connexion to REETI impossible");
		}
	}
	
	/* We wait that our application has received the header sent by URBI to proceed:
	 * [00990763] *** ********************************************************
	 * [00990771] *** Urbi SDK version 2.6 rev. d949607
	 * [00990772] *** Copyright (C) 2004-2011 Gostai S.A.S.
	 * [00990772] *** ....
	 * [00990773] *** Check our community site: http://www.urbiforge.org.
	 * [00990773] *** ********************************************************
	 */
	public static void waitHeaderReception(){
		int starLineCount =0;
		try{
			
			while(starLineCount <= 1){
				while(!in.ready()){
				}
				String line = in.readLine();
				if(line.contains("*********")) starLineCount++;
			}
			System.out.println("Header Received");
		}
		catch(Exception e){
			System.out.println(e);
		}
	}
	
	/*Send the command to Urbi and wait for it to reply*/
	public static String sendMessageAndWaitForReply(String _msg){
		try{
			out.print(_msg+"\n");
			out.flush();
			String line = in.readLine();
			/*Remove the time stamp of the Urbi's answer before returning it*/
			return line.substring(11);
		}
		catch(Exception e){
			System.out.println(e);
			return "error";	
		}
	}
	/*Send the command to Urbi*/
	public static void sendMessage(String _msg){
		try{
			out.print(_msg+"\n");
			out.flush();
		}
		catch(Exception e){
			System.out.println(e);	
		}
	}
	
	/* Will print periodically the position, and stop if the command finished */
	public static void pollServoPosition(){
		float position = Float.parseFloat(sendMessageAndWaitForReply("Global.servo.leftEyePan;"));
		if(position < 99.9){
			System.out.println("Eye pan : " + position);
			/* We wait some time before asking the position again*/
			TimerTask task = new TimerTask()
			{
				@Override
				public void run() 
				{
					pollServoPosition();
				}	
			};
			Timer timer = new Timer();
			timer.schedule(task, 200);
		}else{
			System.out.println("Ready");
			System.exit(0);
		}
	}
}
