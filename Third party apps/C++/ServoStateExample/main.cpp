#include <QCoreApplication>
#include <QDebug>
#include <QTcpSocket>

QTcpSocket m_client;


/* We wait that our application has received the header sent by URBI to proceed:
 * [00990763] *** ********************************************************
 * [00990771] *** Urbi SDK version 2.6 rev. d949607
 * [00990772] *** Copyright (C) 2004-2011 Gostai S.A.S.
 * [00990772] *** ....
 * [00990773] *** Check our community site: http://www.urbiforge.org.
 * [00990773] *** ********************************************************
 */
void WaitHeaderReception()
{
    int starLineCount = 0;
    while(starLineCount <=1){
        m_client.waitForReadyRead(1000);
        while(m_client.canReadLine()){
            QByteArray line = m_client.readLine();
            QString strLine(line);
            if(strLine.contains(QString("*********"))) starLineCount++;
        }
    }
    qDebug()<<"Header Received";
}

/*Creation of the TCPIP connexion with URBI*/
void connectToHost(const QString &_host, int _port){
    m_client.connectToHost(_host,_port,QIODevice::ReadWrite);
    if(m_client.waitForConnected(1000)){
        qDebug()<<"connection established";
        WaitHeaderReception();
    }else{
        qDebug()<<"connection to REETI impossible";
    }
}

/*Send the command to Urbi and wait for it to reply*/
QString sendMessageAndWaitForReply(const char *_msg){
    m_client.write(_msg);
    QString answer;
    if(!m_client.waitForReadyRead(1000)){
        return "connection problem";
    }else{
        while(m_client.canReadLine()){
            answer+=m_client.readLine();
        }
    }
    /*Remove the timespamp and \n of the Urbi's answer before returning it*/
    return answer.mid(11).simplified();
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    /* Connexion to Urbi, by default the Urbi's port is set to 54001 */
    connectToHost("localhost",54001);
    const char *c_str;

    /* Definition of the command to Send to Urbi.
     * In order to retrieve the variable it must have been binded
     * in your UModule using UBindVar(UModuleName,VarName) */
    QString Command = QString ("Global.servo.neckRotat;");
    QByteArray charCommand = Command.toLocal8Bit();
    c_str = charCommand.data();

    /* We ask Urbi about the Variable and print the answer */
    qDebug()<<"Neck Rotation: "+sendMessageAndWaitForReply(c_str);

    Command = QString("Global.servo.neckTilt;");
    charCommand = Command.toLocal8Bit();
    c_str = charCommand.data();
    qDebug()<<"Neck Tilt: "+sendMessageAndWaitForReply(c_str);

    Command = QString("Global.servo.neckPan;");
    charCommand = Command.toLocal8Bit();
    c_str = charCommand.data();
    qDebug()<<"Neck Pan: "+sendMessageAndWaitForReply(c_str);

    Command = QString("Global.servo.leftEar;");
    charCommand = Command.toLocal8Bit();
    c_str = charCommand.data();
    qDebug()<<"Left Ear: "+sendMessageAndWaitForReply(c_str);

    Command = QString("Global.servo.rightEar;");
    charCommand = Command.toLocal8Bit();
    c_str = charCommand.data();
    qDebug()<<"Right Ear: "+sendMessageAndWaitForReply(c_str);

    Command = QString("Global.servo.color;");
    charCommand = Command.toLocal8Bit();
    c_str = charCommand.data();
    qDebug()<<"Cheeks Color: "+sendMessageAndWaitForReply(c_str);
    exit(0);
    return a.exec();
}


