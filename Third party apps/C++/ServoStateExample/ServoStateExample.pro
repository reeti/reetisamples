#-------------------------------------------------
#
# Project created by QtCreator 2015-06-11T09:17:26
#
#-------------------------------------------------

QT       += core network

QT       -= gui

TARGET = ServoStateExample
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
