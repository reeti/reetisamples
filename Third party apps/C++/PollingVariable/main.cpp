#include <QCoreApplication>
#include <QTcpSocket>
#include <QTime>
#include <unistd.h>
QTcpSocket m_client;

/*
 * Simple polling example.
 * This will connect to the urbi console server, move the the left eye servo motor to the left, then slowly move it to the right.
 * During the movement, it will print periodically the position of the servo.
 */



/* We wait that our application has received the header sent by URBI to proceed:
 * [00990763] *** ********************************************************
 * [00990771] *** Urbi SDK version 2.6 rev. d949607
 * [00990772] *** Copyright (C) 2004-2011 Gostai S.A.S.
 * [00990772] *** ....
 * [00990773] *** Check our community site: http://www.urbiforge.org.
 * [00990773] *** ********************************************************
 */
void WaitHeaderReception()
{
    int starLineCount = 0;
    while(starLineCount <=1){
        m_client.waitForReadyRead(1000);
        while(m_client.canReadLine()){
            QByteArray line = m_client.readLine();
            QString strLine(line);
            if(strLine.contains(QString("*********"))) starLineCount++;
        }
    }
    qDebug()<<"Header Received";
}

/*Creation of the TCP/IP connexion with URBI*/
void connectToHost(const QString &_host, int _port){
    m_client.connectToHost(_host,_port,QIODevice::ReadWrite);
    if(m_client.waitForConnected(1000)){
        qDebug()<<"connexion established";
        WaitHeaderReception();
    }else{
        qDebug()<<"connexion to REETI impossible";
    }
}

/*Send the command to Urbi and wait for its reply*/
QString sendMessageAndWaitForReply(QByteArray _msg){
    m_client.write(_msg);
    QString answer;
    if(!m_client.waitForReadyRead(5000)){
        return "connection problem";
    }else{
        while(m_client.canReadLine()){
            answer+=m_client.readLine();
        }
    }
    /*Remove the timespamp and \n of the Urbi's answer before returning it*/
    return answer.mid(11).simplified();
}

/*Send the command to Urbi*/
void sendMessage(QByteArray _msg){
    m_client.write(_msg);
    m_client.flush();
}

/* Poll leftEyePan position */
void isReetiPlaying()
{
    while(true)
    {
        /* Read each 200ms the position of the servo. Return if position > 99.9*/
        bool ok;
        QString valueStr = sendMessageAndWaitForReply("Global.servo.leftEyePan;");
        //qDebug() << "get : "<< valueStr;
        float val = valueStr.toFloat(&ok);
        if(ok && val < 99.9)
        {
            qDebug() << "eye pan : " << val;
            usleep(200000); // 200ms
        }else{
            // ready
            qDebug() << "Ready";
            return;
        }
    }
}


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QString hostname = "127.0.0.1"; // Enter IP address of reeti here

    /* Connection to Urbi, by default the Urbi's port is set to 54001 */
    connectToHost(hostname,54001);
    sendMessageAndWaitForReply("Global.servo.leftEyePan = 0 speed: 50;echo(1);");
    /* Launch a Sequence for the Reeti to play */
    sendMessage("Global.servo.leftEyePan = 100 speed: 25,");
    /* Start the polling */
    isReetiPlaying();
    return 0;
}
