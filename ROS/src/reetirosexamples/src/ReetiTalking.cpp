#include "ros/ros.h"
#include "reetiros/Say.h"
#include "std_msgs/String.h"
#include "std_msgs/Bool.h"
#include <cstdlib>


/*
 * This samples is a ros node which subscribes to the isSpeaking and BookMark publishers.
 * Those publishers allows to be informed when a reeti is speaking or when a bookmark is found in the text.
 * The node also call the SayWithSynchro service in order to make the reeti talk.
 */

/*
 * Creation of the callback linked to the subscriber in order to know when the Reeti is talking
 * */
void isSpeaking(const std_msgs::Bool _isSpeaking)
{
    std::cout << "Reeti Speaking: "<< _isSpeaking.data << std::endl;
}
/*
 * Creation of the services that will get the information from reetiros
 * */
void GetBookmark(const std_msgs::String _book)
{
    std::cout << "Found Bookmark: " << _book.data << std::endl;
}


int main(int argc,char **argv)
{
    /* ROS Initialisation and creation of the main rose node*/
    ros::init(argc,argv,"ReetiTalking");
    ros::NodeHandle n;

    /* Subscrive to the isSpeaking Publisher */
    ros::Subscriber isSpeakingSubscriber = n.subscribe("isSpeaking",10,isSpeaking);
    ros::Subscriber GetBookMarkSubsciber = n.subscribe("BookMark",10,GetBookmark);


    /* Creation of client which will communicate with the choosen service here : Say*/
    ros::ServiceClient SayWithSynchroClient = n.serviceClient<reetiros::Say>("SayWithSynchro");

    /* Creation of a srv which will be sent by the above Client */
    reetiros::Say saysrv;

    /* The text sent to the REETI, bookmarks needs to be 4 char long at least */
    saysrv.request.speech = "Bonjour, je m'appelle Reeti \\book=name , je test actuellement\\book=current les bookmarks et detecte quand j'arrete de paler";

    if(SayWithSynchroClient.call(saysrv)){
        ROS_INFO("Making reeti talk and moving its Lips;");
    }else{
        ROS_ERROR("Failed to call service SayWithSynchro");
    }

    ros::spin();
    return 0;
}
