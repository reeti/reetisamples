#include "ros/ros.h"
#include "reetiros/Camera.h"

/*
 * This sample is a ros node which call the services TakePicture, RecordVideo and StopRecord
 */

int main(int argc,char **argv){
    /* ROS Initialisation and creation of the main rose node*/
    ros::init(argc,argv,"ReetiTalkingExample");
    ros::NodeHandle n;

    /* Creation of client which will communicate with the choosen service here : TakePicture, RecordVideo and StopRecord*/
    ros::ServiceClient TakePictureClient = n.serviceClient<reetiros::Camera>("TakePicture");
    ros::ServiceClient RecordVideoClient = n.serviceClient<reetiros::Camera>("RecordVideo");
    ros::ServiceClient StopRecordClient = n.serviceClient<reetiros::Camera>("StopRecord");

    /* Creation of a srv which will be sent by the above Client */
    reetiros::Camera camsrv;

    /* The parameters send through the Client */
    camsrv.request.camera = 1;
    camsrv.request.filename = "testPicture";

    if(TakePictureClient.call(camsrv)){
        ROS_INFO("Taking a Picture");
    }else{
        ROS_ERROR("Failed to call service TakePicture");
    }
    ros::Duration(1).sleep();

    camsrv.request.camera = 1;
    camsrv.request.filename = "testVideo";

    if(RecordVideoClient.call(camsrv)){
        ROS_INFO("Recording a Video");
    }else{
        ROS_ERROR("Failed to call service RecordVideo");
    }

    ros::Duration(6).sleep();
    camsrv.request.camera = 1;

    if(StopRecordClient.call(camsrv)){
        ROS_INFO("Stopping the Recording");
    }else{
        ROS_ERROR("Failed to call service StopRecord");
    }

    return 0;
}
