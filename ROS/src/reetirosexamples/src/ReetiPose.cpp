#include "ros/ros.h"
#include "reetiros/Pose.h"
#include "reetiros/ledColor.h"
#include "std_msgs/String.h"
#include <cstdlib>

/*
 * This sample is a ros node which subscribe to the Reeti servo's positions.
 * The program start by calling the service setPose which set all the servo positions.
 * Then, you can make your reeti moving with RPilot, and you will see the callback "GetPosition" triggered.
 */

void GetPosition(const reetiros::reetiPose _pose)
{
    std::cout <<"Neck rotation: " << _pose.neckRotat
             <<"\nNeck pan: " << _pose.neckPan
            <<"\nNeck tilt: "<< _pose.neckTilt
           <<"\nMouth Right Corner: "<< _pose.rightLC
          <<"\nMouth Left Corner: " << _pose.leftLC
         <<"\nTop Lip: "<< _pose.topLip
        <<"\nBottom Lip: "<< _pose.bottomLip
       <<"\nRight eye pan: "<< _pose.rightEyePan
      <<"\nRight eye Tilt: "<< _pose.rightEyeTilt
     <<"\nLeft eye pan: "<< _pose.leftEyePan
    <<"\nLeft eye Tilt: "<< _pose.leftEyeTilt
    <<"\nRight eye lid: "<< _pose.rightEyeLid
    <<"\nLeft eye lid:"<< _pose.leftEyeLid
    <<"\nRight ear: "<< _pose.rightEar
    <<"\nLeft ear: "<< _pose.leftEar << std::endl;
}


int main(int argc,char **argv)
{
    /* ROS Initialisation and creation of the main rose node*/
    ros::init(argc,argv,"ReetiPose");
    ros::NodeHandle n;

    /* Subscrive to the Position Publisher */
    ros::Subscriber PositionSubscriber = n.subscribe("Position",1000,GetPosition);
    /* Creation of client which will communicate with the choosen service here : SetPose and GetPose*/
    ros::ServiceClient SetPoseClient = n.serviceClient<reetiros::Pose>("SetPose");

    /* Creation of a srv which will be sent by the above Client */
    reetiros::Pose setposesrv;

    /* Setting up the requested motors' positions */
    setposesrv.request.pose.neckRotat = 20;
    setposesrv.request.pose.neckPan= 20;
    setposesrv.request.pose.neckTilt= 20;
    setposesrv.request.pose.rightLC= 20;
    setposesrv.request.pose.leftLC= 20;
    setposesrv.request.pose.topLip= 20;
    setposesrv.request.pose.bottomLip= 20;
    setposesrv.request.pose.rightEyePan= 20;
    setposesrv.request.pose.rightEyeTilt= 20;
    setposesrv.request.pose.leftEyePan= 20;
    setposesrv.request.pose.leftEyeTilt= 20;
    setposesrv.request.pose.rightEyeLid= 20;
    setposesrv.request.pose.leftEyeLid= 20;
    setposesrv.request.pose.rightEar= 20;
    setposesrv.request.pose.leftEar= 20;
    setposesrv.request.speed = 25;

    if(SetPoseClient.call(setposesrv)){
        ROS_INFO("Setting Reeti to position.");
    }else{
        ROS_ERROR("Failed to call service SetPosition.");
    }
    ros::spin();
    return 0;
}
