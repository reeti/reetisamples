#include "ros/ros.h"
#include "reetiros/Sequence.h"
#include "std_msgs/Bool.h"

/*
 * This samples is a ros node which allow to call the PlayPose and PlaySequence services.
 * Each services allow to respectively launch an existing pose or sequence on the REETI.
 */

int main(int argc,char **argv){
    /* ROS Initialisation and creation of the main rose node*/
    ros::init(argc,argv,"ReetiPlayer");
    ros::NodeHandle n;
    /* Creation of client which will communicate with the choosen service here : PlayPose, PlaySequence and StopCurrentSequence*/
    ros::ServiceClient PlayPoseClient = n.serviceClient<reetiros::Sequence>("PlayPose");
    ros::ServiceClient PlaySequenceClient = n.serviceClient<reetiros::Sequence>("PlaySequence");
    /* Creation of a srv which will be sent by the above Client */
    reetiros::Sequence seqsrv;

    /* The parameters send through the Client */
    seqsrv.request.sequence = "Ears/earsBottom";

    if(PlayPoseClient.call(seqsrv)){
        ROS_INFO("Sending a pose to Reeti.");
    }else{
        ROS_ERROR("Failed to call service PlayPose.");
    }

    ros::Duration(2).sleep();
    seqsrv.request.sequence = "ReetiSeq/dance";

    if(PlaySequenceClient.call(seqsrv)){
        ROS_INFO("Sending a sequence to Reeti.");
    }else{
        ROS_ERROR("Failed to call service PlaySequence.");
    }
    
    return 0;
}
