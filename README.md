# README #

* This repository gives some code samples for **Reeti** robot Development.
* Each sample has its own README for compiling and running instructions.
* Refer to reeti for developer instructions : **http://wiki.reeti.fr**
 

* Visit our website www.reeti.fr for more informations about our expressive robot.