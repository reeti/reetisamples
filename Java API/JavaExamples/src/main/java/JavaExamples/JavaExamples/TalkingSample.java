package JavaExamples.JavaExamples;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import reeti.*;
import reeti.Reeti.ReetiBookmark;
import reeti.Reeti.ReetiSpeaking;

/*
 * This samples subscribes to the isSpeaking and BookMark publishers.
 * Those allows to be informed when a reeti is speaking or when a bookmark is found in the text.
 * The node also call the SayWithSynchro service in order to make the reeti talk.
 */

public class TalkingSample {
	public static void main(String[] args) throws IOException{
		/* Create an instance of reeti and connect to the chosen reeti */
		final Reeti reeti = new Reeti("127.0.0.1");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		/* Check if the connection is done */
		if(reeti.isConnected()){
			reeti.subscribeToBookmark(new BookmarkCallback(){
				public void onBookmark(ReetiBookmark _book) {
					String bookmark = "Found Bookmark: " + _book.bookmark;
					System.out.println(bookmark);
				}
			});
			reeti.subscribeToIsSpeaking(new SpeakingCallback(){
				public void onIsSpeaking(ReetiSpeaking _isSpeaking) {
					String speaking = "Reeti Speaking: " + _isSpeaking.isSpeaking;
					System.out.println(speaking);
				}	
			});
			reeti.sayWithSynchroAsync("Bonjour, je m'appelle Reeti \\\\book=name , je test actuellement\\\\book=current les bookmarks et detecte quand j'arrete de parler", new ServicesCallback(){
				public void onServiceFinished() {
					System.out.println("Service OK");
				}	
			});
		}
		br.readLine();
	}
}
