package JavaExamples.JavaExamples;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import reeti.*;
import reeti.Reeti.*;

/*
 * This sample subscribes to the Reeti servo's positions.
 * The program start by setting all the servo positions.
 * Then, you can make your reeti moving with RPilot, and you will see the callback "GetPosition" triggered.
 */

public class PoseAsync {
	static ReetiPosition prevPos;
	public static void main(String[] args) throws IOException{
		/* Create an instance of reeti and connect to the chosen reeti */
		final Reeti reeti = new Reeti("127.0.0.1");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		/* Check if the connection is done */
		if(reeti.isConnected()){
			reeti.subscribeToPosition(new PositionCallback(){
				public void onPosition(ReetiPosition _pos) {
					if(_pos.equals(prevPos)){
					}else{
						String position = "neckRotat: " + _pos.neckRotat + "\n"
				                 +"neckPan: " + _pos.neckPan + "\n"
				                 +"neckTilt: " + _pos.neckTilt + "\n"
				                +"rightLC: " + _pos.rightLC + "\n"
				               +"leftLC: " + _pos.leftLC + "\n"
				              +"topLip: " + _pos.topLip + "\n"
				             +"bottomLip: " + _pos.bottomLip + "\n"
				            +"rightEyePan: " + _pos.rightEyePan + "\n"
				           +"rightEyeTilt: " + _pos.rightEyeTilt + "\n"
				          +"leftEyePan: " + _pos.leftEyePan + "\n"
				         +"leftEyeTilt: " + _pos.leftEyeTilt + "\n"
				         +"rightEyeLid: " + _pos.rightEyeLid + "\n"
				         +"leftEyeLid: " + _pos.leftEyeLid + "\n"
				         +"rightEar: " + _pos.rightEar + "\n"
				         +"leftEar: " + _pos.leftEar;
						System.out.println(position);
						prevPos=_pos;
					}
				}
			});
			ReetiPosition pos = reeti.new ReetiPosition(20,20,20,20,20,20,20,20,20,20,20,20,20,20,20);
			reeti.setPoseAsync(pos, 25, new ServicesCallback(){
				public void onServiceFinished() {
					System.out.println("Service Ok");
				}
			});
		}
		br.readLine();
	}
}
