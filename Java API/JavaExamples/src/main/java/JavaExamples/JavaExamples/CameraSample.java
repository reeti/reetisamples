package JavaExamples.JavaExamples;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import reeti.*;
import reeti.Reeti.*;

/*
 * This sample show you how to take a picture, record a video and stop the recording
 */

public class CameraSample {
	public static void main(String[] args) throws IOException{
		/* Create an instance of reeti and connect to the chosen reeti */
		final Reeti reeti = new Reeti("127.0.0.1");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		/* Check if the connection is done */
		if(reeti.isConnected()){
			reeti.takePictureAsync(Camera.RIGHTCAM, "picturejavatest",new ServicesCallback(){
				public void onServiceFinished() {
					System.out.println("Service Ok");
				}
			});
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			reeti.recordVideoAsync(Camera.LEFTCAM, "videojavatest", new ServicesCallback(){
				public void onServiceFinished() {
					System.out.println("Service Ok");
					/* If the record video is launched in the main we wait some time and stop it after */
					try {
						Thread.sleep(4000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					reeti.stopRecordAsync(Camera.LEFTCAM, new ServicesCallback(){
						public void onServiceFinished() {
							System.out.println("Service Ok");
						}
					});
				}
			});
		}
		br.readLine();
	}
}
