package JavaExamples.JavaExamples;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import reeti.*;
import reeti.Reeti.ReetiBookmark;
import reeti.Reeti.ReetiLedColor;
import reeti.Reeti.*;
/* A simple example that show how to use the asynchronous functions of the reeti api.
 * The example will make the reeti move wait for him to reach a position, then make
 * him talk until a bookmark is reached and then change the led color.
 * */

public class GeneralExample {
	
	static int count =0;
	
	public static void main(String[] args) throws IOException{
		/* Create an instance of reeti and connect to the chosen reeti */
		final Reeti reeti = new Reeti("127.0.0.1");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		/* Check if the connection is done */
		if(reeti.isConnected()){
			/* Creation of a ReetiPosition to store the position of the 15 actuators */
			ReetiPosition pos = reeti.new ReetiPosition(5,50,50,50,50,50,50,50,50,50,50,100,100,50,50);
			reeti.subscribeToPosition(new PositionCallback(){
				public void onPosition(ReetiPosition _pos) {
					if(_pos.neckRotat < 6 && count ==0){
						count = 1;
				        /* make the reeti talk and move its lips */
						reeti.sayWithSynchroAsync("\\language=English \\voice=Simon I reached the position, I will now \\book=color change my led color", new ServicesCallback(){
							public void onServiceFinished() {
								System.out.println("Service Ok");
							}
						});
					}
				}
			});
			reeti.subscribeToLedColor(new LedColorCallback(){
				public void onLedColor(ReetiLedColor _color) {
					if(_color.color.compareTo("red")==0){
						reeti.sayWithSynchroAsync("\\language=English \\voice=Simon I do prefer the green color",new ServicesCallback(){
							public void onServiceFinished() {
								System.out.println("Service Ok");
							}
						});
						try {
							Thread.sleep(3000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						reeti.setLedColor(Led.BOTHLED,"green");
					}
				}
			});
			reeti.subscribeToBookmark(new BookmarkCallback(){
				public void onBookmark(ReetiBookmark _book) {
					if(_book.bookmark.compareTo("color")==0){
						 /* set the led color */
				        reeti.setLedColor(Led.RIGHTLED,"blue");
				        reeti.setLedColor(Led.LEFTLED,"red");
					}
				}		
			});
			reeti.setPoseAsync(pos,40,new ServicesCallback(){
				public void onServiceFinished() {
					System.out.println("Service Ok");
				}
			});
		}
		br.readLine();
	}
}
