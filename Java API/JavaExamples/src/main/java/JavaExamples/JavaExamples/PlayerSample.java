package JavaExamples.JavaExamples;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import reeti.*;

/*
 * This samples allow to  launch an existing pose or sequence on the REETI.
 */

public class PlayerSample {

	public static void main(String[] args) throws IOException{
		/* Create an instance of reeti and connect to the chosen reeti */
		final Reeti reeti = new Reeti("127.0.0.1");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		/* Check if the connection is done */
		if(reeti.isConnected()){
			reeti.playPoseAsync("Ears/earsBottom", new ServicesCallback(){
				public void onServiceFinished() {
					System.out.println("Service Ok");
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					reeti.playSequenceAsync("ReetiSeq/dance", new ServicesCallback(){
						public void onServiceFinished() {
							System.out.println("Service Ok");
						}
					});
				}
			});
		}
		br.readLine();
	}
}
