package reeti;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import urbi.UClient;
import urbi.urbijava;

public class ChangeLedColor {
	private static UClient client;

	public static void main(String[] args) {

		String IP = null;
		try
		{
			/// Load the c++ native library.
			System.loadLibrary("urbijava");
			System.out.println("---------- URBI LIB LOADED ----------");
		}
		catch (java.lang.UnsatisfiedLinkError e)
		{
			System.out.println("ERROR LOAD URBI JAVA");

			System.out.println ("Loading exception: " + e);

			// DEBUG INFORMATIONS
			System.out.println();
			System.out.println("----------- Java.library.path -------------");
			String lib_path = System.getProperty("java.library.path");
			System.out.println(lib_path);

			// get the system environment variables
			System.out.println();
			System.out.println("---------- ENVIRONMENT VARIABLES ----------");
			Map<String, String> envMap = System.getenv();
			// print the system environment variables
			for (String key : envMap.keySet())
				System.out.println(key + " = " +  envMap.get(key));
			System.out.println();

			System.exit (1);
		}
		
		if(args.length >= 1)
		{
			IP = args[0];
		}
		else
		{
			IP = "127.0.0.1";
		}
		
		
		System.out.println("connecting ...");
	    client = urbijava.connect(IP);
		
		client.send("Global.servo.changeLedColor(\"red\");");
		try {
			System.out.println("waiting 5secs...");
			TimeUnit.SECONDS.sleep(5);
			System.out.println("Exiting");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
}
