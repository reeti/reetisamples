How to compile :
- open this project in eclipse (File -> Import -> Existing projects)
- build

How to launch :
- eclipse : 
  * open "ChangeLedColor" properties, then choose "Run/Debug Settings" -> "Edit" -> "Arguments"
  * in the text field "VM arguments", add : "-Djava.library.path=/usr/local/gostai/lib/"
  * press the launch button
- command line : 
   * cd bin
   * java -cp /usr/local/gostai/share/sdk-remote/java/lib/liburbijava.jar:. -Djava.library.path=/usr/local/gostai/lib/ reeti.ChangeLedColor 127.0.0.1

