#include "urbi/uclient.hh"
#include <iostream>
#include <string.h>
#include <stdio.h>
using namespace std;
using namespace urbi;

int main(int argc, char** argv)
{
	UClient * client;
	if(argc < 2)
	{
		cout << "usage : " << argv[0] << " <IP> " << endl;
		return 0;
	}
	client=new UClient(argv[1], 54001);
	string cmd = "Global.servo.changeLedColor(\"red\");";
	client->send("%s", cmd.c_str());
	sleep(1);
	cmd = "Global.servo.changeLedColor(\"blue\");";
	client->send("%s", cmd.c_str());
	sleep(1); // wait some time before disconnecting
	return 1;
}
