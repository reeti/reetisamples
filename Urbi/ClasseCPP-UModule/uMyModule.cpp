#include"uMyModule.h"

using namespace std;

// We declare our class in Urbi
UStart(MyModule);

// Class constructor
MyModule::MyModule(const string &n):UObject(n)
{
    UBindFunction(MyModule, init) ;
}

// called by Urbi onthe creation of this object
int MyModule::init()
{
    // Make the play function accessible by Urbi script
    UBindFunction(MyModule, play) ;
    // Make the color variable accessible by Urbi script
    UBindVar(MyModule, color) ;
    // Connect a callback on change of our UVar
    UNotifyChange(color, &MyModule::OnColorChange);
	return 0;
}

// can be called from urbi
int MyModule ::play(string _str)
{
	if(_str=="ears") MoveEars();
	else cout<<"I don't understand your order. Try play(\"ears\")"<<endl;
	return 1;
}

// move urbi ears
void MyModule::MoveEars()
{
    // evaluate urbi script
	send("Global.servo.rightEar=100;");
	send("Global.servo.leftEar=100;");
}

// called when the color has changed
int MyModule::OnColorChange(UVar& _color)
{
    send("Global.servo.color=\"" + static_cast<std::string>(_color) + "\";");
}
