How to compile :

- cd build
- cmake ..
- make

How to test :
In an urbi console, enter the following instructions :

System.loadModule("/home/reeti/reetiDevel/Samples/Urbi/ClasseCPP-UModule/build/libMyModule.so");
var Global.MyModule = uobjects.MyModule.new();
Global.MyModule.play("ears");
Global.MyModule.color = "blue";


Load the module on startup :
http://wiki.reeti.fr/display/REETI/Run+your+module+at+Reeti+Launch

