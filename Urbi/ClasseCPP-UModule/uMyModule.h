#ifndef MY_MODULE_H
#define MY_MODULE_H
#include <string.h>
#include <iostream>
#include <stdio.h>

// Include urbi headers
#include<urbi/uobject.hh>
#include<urbi/uclient.hh>

using namespace urbi;

//our class inherits UObject
class MyModule:public UObject{
public:

    // default constructor for an UObject
    MyModule(const std::string &n);

    // called by Urbi onthe creation of this object
    int init();

    // we declare a function that can be called by urbi scripts
    int play(std::string _str);

private: 
    // an function that is not accessible by urbi script
    void  MoveEars();

    // an Urbi variable
    UVar color;

    // Callback called when the UVar color is changed
    int OnColorChange(UVar& var);
};
#endif
