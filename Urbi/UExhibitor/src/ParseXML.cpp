#include "ParseXML.h"
#include <iostream>
using namespace std;

void FillWithRes(xmlXPathContextPtr& _ctxt, string _xpathExpression, vector<string>& _outFill, bool _mustBePresent)
{
    bool ok = false;
    xmlXPathObjectPtr xpathRes = xmlXPathEvalExpression((const xmlChar*)_xpathExpression.c_str(), _ctxt);
    if(xpathRes!=NULL)
    {
        if (xpathRes->type == XPATH_NODESET)
        {
            if(xpathRes->nodesetval)
            {
                if(xpathRes->nodesetval->nodeNr == 0) throw std::runtime_error("There is no "+string(_xpathExpression)+" in XML File");
                for (int i = 0; i < xpathRes->nodesetval->nodeNr; i++)
                {
                    xmlNodePtr n = xpathRes->nodesetval->nodeTab[i];
                    if (n->type == XML_TEXT_NODE || n->type == XML_CDATA_SECTION_NODE)
                    {
                        _outFill.push_back(string((const char*)n->content));
                    }
                }
            }
        }
        xmlXPathFreeObject(xpathRes);
    }
    if(!ok && _mustBePresent)throw std::runtime_error(string("Can't find node")+_xpathExpression);
}

