/**
\author aba
\date 27/03/2012
*/
#include "UExhibitor.h"

#include <boost/thread/mutex.hpp>
#include <boost/bind.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/foreach.hpp>
#include <boost/filesystem.hpp>
#include <sstream>
#include <fstream>
#include <iostream>
#include <libxml/xpath.h>
#include "ParseXML.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <stdio.h>

using namespace urbi;
using namespace std;
using namespace cv;

#define FRAME_DELAY     0.03

UExhibitorSample::UExhibitorSample(const string &_name):
    UObject(_name),
    m_thread(NULL)
{
    UBindFunction(UExhibitorSample, init);
}

UExhibitorSample::~UExhibitorSample(void)
{
    if(m_thread)
    {
        Stop();
    }
}

void UExhibitorSample::ReadSequencesFolder(const string &_folder)
{
    // The function "playSequence" of UPlayer needs a string formatted in the following way :
    // "/home/reeti/reetiDocuments/Sequences/<arg>.rmdl"
    // So "_folder" has to be a path relative to "/home/reeti/reetiDocuments/Sequences"
    // For example if _folder is "ShaktiDemo", we will search all *.rmdl files in
    // "/home/reeti/reetiDocuments/Sequences/ShaktiDemo".
    std::string baseFolder("/home/reeti/reetiDocuments/Sequences/");
    baseFolder.append(_folder);
    // On itère sur tous les fichiers du répertoire :
    for(boost::filesystem::directory_iterator itr(baseFolder); itr!=boost::filesystem::directory_iterator(); ++itr)
    {
        if (boost::filesystem::is_regular_file(itr->status()))
        {
            if(itr->path().extension()==".rmdl")
            {
                std::string sequenceName = _folder;
                sequenceName.append("/");
                std::string fnameWithExtension = itr->path().filename().string();
                std::string fnameWithoutExtension = fnameWithExtension.substr(0, fnameWithExtension.size()-5);
                sequenceName.append(fnameWithoutExtension);
                m_callOutSequences.push_back(sequenceName);
                m_durations[sequenceName] = GetDuration(itr->path().native());
            }
        }
    }
}

int UExhibitorSample::init(int _camera)
{
    m_camera = _camera;
    m_shouldStop = false;
    UBindFunction(UExhibitorSample, Start);
    UBindFunction(UExhibitorSample, Stop);
    UBindFunction(UExhibitorSample, GetDuration);
    UBindFunction(UExhibitorSample, StartFromConfigFile);

    //std::cout << "UExhibitor init ready !" << std::endl;

    return 0;
}

void UExhibitorSample::StartFromConfigFile()
{
    if(m_thread==NULL)
    {
        // First of all we read the configuration file
        xmlDocPtr document = xmlParseFile("/reetiPrograms/configuration/uexhibitor.xml");
        // We use XPath to find all the CallOutSequences
        xmlXPathInit();
        xmlXPathContextPtr ctxt = xmlXPathNewContext(document);
        if (ctxt == NULL) throw std::runtime_error("Can't parse XML File");
        std::string folder;
        double ratio, factor_t;
        int count, offset_t;
        bool verbose;

        FillWithRes<double>(ctxt, string("/config/ratio/text()"), ratio, true);
        FillWithRes<double>(ctxt, string("/config/factor_t/text()"), factor_t, true);
        FillWithRes<int>(ctxt, string("/config/count/text()"), count, true);
        FillWithRes<int>(ctxt, string("/config/offset_t/text()"), offset_t, true);
        FillWithRes<bool>(ctxt, string("/config/verbose/text()"), verbose, true);
        FillWithRes<std::string>(ctxt, string("/config/folder/text()"), folder, true);
        // Free the memory
        xmlXPathFreeContext(ctxt);

        Start(folder, ratio, count, verbose, offset_t, factor_t);
    }// If m_thread != NULL, we already started : do nothing !
}

void UExhibitorSample::Start(string _folder, double _ratio, int _count, bool _verbose, int _offset_t, double _factor_t)
{
    if(m_thread==NULL)
    {
        ReadSequencesFolder(_folder);
        m_thread = new boost::thread(boost::bind(&UExhibitorSample::Run, this, _ratio, _count, _verbose,
                                                                _offset_t, _factor_t));
    }
    // Else, the thread is already running, we do nothing.
}

bool UExhibitorSample::Stop(void)
{
    {
        boost::mutex::scoped_lock (m_mxShouldStop);
        m_shouldStop = true;
    }
    m_thread->join();
    delete m_thread;
    m_thread = NULL;
    cout << "UExhibitor.Finish" << endl;
    return true;
}

void UExhibitorSample::Run(float _ratio, int _consecutive, bool _verbose, int _offset_t, double _factor_t)
{
    {
        boost::mutex::scoped_lock (m_mxShouldStop);
        m_shouldStop = false;
    }
    if(_verbose) std::cout << "Starting Exhbitor with ratio : " << _ratio << " and consecutive : " << _consecutive << std::endl;
    // Creating video capture.
    m_cap.open(m_camera);
    if(!m_cap.isOpened())
    {
        cout << "Can't open camera" << endl;
        return;
    }
    Mat tmp_frame;
    m_cap >> tmp_frame;
    if(!tmp_frame.data)
    {
        cout << "cannot read data from the video source" << endl;
        return;
    }
    while(!ShouldThreadStop())
    {
        if(WaitSomethingMove(_ratio, _consecutive, _verbose))
        {
            std::string seqSent = ChooseASequenceIn(m_callOutSequences);
            std::string cmd = string("Global.playSequence(\"")+seqSent+string("\");");
            std::cout << "Starting sequence : " << seqSent << std::endl;
            send(cmd);
            std::cout << "Wait for acquiring : " << m_durations[seqSent]*_factor_t+_offset_t << std::endl;
            WaitForAcquiring(m_durations[seqSent]*_factor_t+_offset_t);
            std::cout << "DONE" << std::endl;
        }
    }
    m_cap.release();
}

bool UExhibitorSample::WaitSomethingMove(float _ratio, int _consecutive, bool _verbose)
{
    if(_verbose) std::cout << "Waiting something move" << std::endl;
    BackgroundSubtractorMOG bgSubstractor;
    Mat tmp_frame, bgmask;
    int consectuif = 0;
    do
    {
        m_cap >> tmp_frame;
        if( !tmp_frame.data ) return false;
        cv::Mat workingImage;
        if(tmp_frame.rows > 520)
        {
            // Image is too big !
            float scaleFactor = 480./static_cast<float>(tmp_frame.rows);
            resize(tmp_frame, workingImage, cv::Size(), scaleFactor, scaleFactor, INTER_AREA);
        }else
        {
            workingImage = tmp_frame;
        }
        bgSubstractor(workingImage, bgmask, -1);
        float ratio_move = static_cast<float>(countNonZero(bgmask))/(bgmask.cols*bgmask.rows);
        if(_verbose) std::cout << "ratio : " << ratio_move << std::endl;
        if(ratio_move > _ratio) consectuif++;
        else consectuif = 0;
    }while(!ShouldThreadStop() && consectuif < _consecutive);
    return consectuif >= _consecutive;
}

double UExhibitorSample::GetDuration(const std::string& _fname)
{
    // In the rmdl file should contain the following part:
    // ";duration=<duration>;"
    // We will search for the value of "<duration>"
    std::fstream file(_fname.c_str(), std::ios_base::in);
    if(!file)
    {
        std::cerr<< "can't find file " << _fname;
        return 0.;
    }

    // using boost regex, this could be simplified
    int seekDataBegin, seekDataEnd;
    string line;
    while(std::getline(file, line))
    {
        seekDataBegin = line.find("duration=");
        seekDataEnd = line.find(";");
        if(seekDataBegin >= 0 && seekDataEnd >= 0)
        {
            int equal = line.find("=");
            std::string duration = line.substr(equal+1, seekDataEnd-(equal+1));
            try
            {
                float res = boost::lexical_cast<float>(duration);
                return res;
            }catch(boost::bad_lexical_cast& )
            {
                return 0.;
            }
        }
    }
    return 0.;
}

bool UExhibitorSample::ShouldThreadStop(void)
{
    boost::mutex::scoped_lock (m_mxShouldStop);
    return m_shouldStop;
}

void UExhibitorSample::WaitForAcquiring(float _seconds)
{
    int delayInLoopCount = static_cast<int>(round(_seconds/FRAME_DELAY));
    Mat tmp;
    while(delayInLoopCount > 0 && !ShouldThreadStop())
    {
        m_cap >> tmp;
        //waitKey(FRAME_DELAY*1000.);
        delayInLoopCount--;
    }
}

string UExhibitorSample::ChooseASequenceIn(tSequences _sequences)
{
    boost::uniform_int<> uniform(0,_sequences.size()-1);
    boost::variate_generator<boost::mt19937&, boost::uniform_int<> > chooser(m_rng, uniform);
    string seqName = _sequences[chooser()];
    return seqName;
}


UStart(UExhibitorSample);
