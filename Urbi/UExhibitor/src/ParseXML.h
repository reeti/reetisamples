#ifndef PARSEXML_H
#define PARSEXML_H
#include <libxml2/libxml/parser.h>
#include <libxml2/libxml/tree.h>
#include <libxml2/libxml/xpath.h>
#include <boost/lexical_cast.hpp>
#include <iostream>
#include <vector>
#include <string>
#include <stdexcept>

/*!
 * Part of the XML parse procedure.
 */
void FillWithRes(xmlXPathContextPtr& _ctxt, std::string _xpathExpression, std::vector<std::string>& _outFill, bool _mustBePresent=false);

template<typename T>
void FillWithRes(xmlXPathContextPtr& _ctxt, std::string _xpathExpression, T& _outFill, bool _mustBePresent=false)
{
    bool ok = false;
    xmlXPathObjectPtr xpathRes = xmlXPathEvalExpression((const xmlChar*)_xpathExpression.c_str(), _ctxt);
    if(xpathRes!= NULL)
    {
        if (xpathRes->type == XPATH_NODESET)
        {
            if(xpathRes->nodesetval)
            {
                if(xpathRes->nodesetval->nodeNr > 1) std::cout << "Warning more than one " << _xpathExpression << " I take the first";
                xmlNodePtr n = xpathRes->nodesetval->nodeTab[0];
                if (n->type == XML_ELEMENT_NODE)
                {
                    xmlChar* str;
                    str = xmlNodeGetContent(n);
                    if(str!=NULL)
                    {
			try
			{
                        	_outFill = boost::lexical_cast<T>((char*)str);
				ok = true;
			}catch(boost::bad_lexical_cast& ) {}
                        xmlFree(str);
                    }
                }else if(n->type == XML_TEXT_NODE)
		{
			try
			{
				_outFill = boost::lexical_cast<T>((char*)n->content);
				ok = true;
			}catch(boost::bad_lexical_cast&){}
		}
            }
            xmlXPathFreeObject(xpathRes);
        }
    }
    if(!ok && _mustBePresent)throw std::runtime_error(std::string("Can't find node")+_xpathExpression);
}

xmlXPathObjectPtr CheckPath(xmlXPathContextPtr& _ctxt, std::string _xpathExpression);

#endif // PARSEXML_H
