/** @defgroup UExhibitor UExhibitor
 * Module qui interpelle quelqu'un passant dans le champs de vision du reeti.
 */

#ifndef UCAMERA_H
#define UCAMERA_H

#include <urbi/uclient.hh>
#include <urbi/uobject.hh>
#include <string>
#include <vector>
#include <boost/thread/mutex.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/background_segm.hpp>

/**
 *\ingroup UExhibitor
 *\class UExhibitor
 *\brief This module will play a sequence when somebody moves in front the reeti
 * - var Global.exhibitor = uobjects.UExhibitor.new(0);
 * - Global.exhibitor.Start("<folder_relative_to_sequence>", <seuil_change>, <consecutive>, false, 5, 1.1);
 * \arg folder_relative_to_sequence : Folder where the sequences will be placed
 *
 * Lorsque le fond de l'image va trop changer, une séquence au hasard sera tirée dans callouts pour être jouée.
 */

class UExhibitorSample: public urbi::UObject
{
    typedef std::vector<std::string> tSequences;
    typedef std::map<std::string, float> tSequencesDurationBySequenceName;
public:
    UExhibitorSample(const std::string& _name);
    int init(int _camera);

    // Thread Safe methods :
    // // Binded Methods :
    /*!
     * If the separated thread of the \c Run method isn't running, this method launch it.
     * This method return as soon as possible. This is a non blocking call.
     * \arg folder_relative_to_sequence : Le répertoire dans le quel on ira piocher les séquences d'interpellations.
     * \arg Le % de pixels non zero dans le masque de BG pour supposé que quelque chose a bougé (float 0:1)
     * \arg le nombre d'images consécutives avec mouvement pour déclancher une interpellation. (uint 1+)
     * \arg _verbose : Affiche ou pas les traces
     * \arg _offset_t et _factor_t : le temps d'attente après la fin d'une séquence est : _factor_t*<duree sequence> + _offset_t.
     */
    void Start(std::string _folder, double _ratio, int _consecutive, bool _verbose, int _offset_t, double _factor_t);

    /*!
     * This function will call \c Start after read the paramters in a configuration XML file.
     */
    void StartFromConfigFile(void);

    /*!
     * This method will set the \c m_shouldStop at true. hence, at the next iteration the \c Run thread will stop.
     * This method join the thread before returning
     */
    bool Stop(void);

    /*!
     * destructor... Stop the thread (if it exists).
     */
    ~UExhibitorSample(void);

    double GetDuration(const std::string& _fname);
private:
    void ReadSequencesFolder(const std::string& _folder);
    /*!
     * This method run in a sperate thread. This method does the image processing
     * This method is started by the \c Start method in the thread \c m_thread. After each iteration, this function
     * checks if it should stop running.
     * \arg _fname a path to the XML file containing the mission description
     */
    void Run(float _ratio, int _consecutive, bool _verbose, int _offset_t, double _factor_t);

    bool WaitSomethingMove(float _ratio, int _consecutive, bool _verbose);

    /*!
     * Wait for \c _seconds s before acquiring a new image.
     */
    void WaitForAcquiring(float _seconds);

    /*!
     * Take one of the callOutSequence and ask the reeti to play IT.
     */
    void CallOut(void);
    std::string ChooseASequenceIn(tSequences _sequences);
    // Thread safe :
    bool ShouldThreadStop(void);

    boost::thread*  m_thread;
    boost::mutex    m_mxShouldStop;
    bool            m_shouldStop;

    /*!
     * non protected variables.
     */
    boost::mt19937  m_rng;
    float           m_percentThreshold;
    int             m_consecutiveImage;
    int             m_camera;
    tSequences      m_callOutSequences;
    tSequencesDurationBySequenceName m_durations;
    cv::VideoCapture    m_cap;
};


#endif
