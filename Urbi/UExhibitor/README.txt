Example of an application using Reeti's webcam

How to compile :

- cd build
- cmake ..
- make

How to test :
In an urbi console, enter the following instructions :

System.loadModule("/home/reeti/reetiDevel/Samples/Urbi/UExhibitor/build/libUExhibitor.so");
var Global.UExhibitorModule = uobjects.UExhibitorSample.new(7); # 6 is left webcam loopback device, 7 is right webcam loopback device
Global.UExhibitorModule.Start("Emotions", 0.05, 10, true, 5, 1.05);
(move something in front of the webcam)
Global.UExhibitorModule.Stop();

